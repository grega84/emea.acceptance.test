package emea.acceptance.test.ui;

import java.io.File;
import java.net.URI;
import java.nio.file.Paths;

public class EmeaUtils 
{
	public static File[] getFilesFromFolder(URI uri)
	{
		File folder;
		try {
			folder = Paths.get(uri).toFile();
			File[] listOfFiles = folder.listFiles();
			
			return listOfFiles;
		}
		catch(Exception err)
		{
			err.printStackTrace();
			return null;
		}
	}
}
