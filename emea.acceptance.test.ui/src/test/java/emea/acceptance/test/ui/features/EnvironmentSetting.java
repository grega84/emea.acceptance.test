package emea.acceptance.test.ui.features;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.*;
import emea.acceptance.test.ui.TestEnvironment;
import emea.automation.core.ui.common.datatable.EnvironmentSettings;
import test.automation.core.UIUtils;
import ui.core.support.Costants;

/**
 * classe contenenti le implementazioni realtive ai feature di configurazione ambiente
 * @author Francesco
 *
 */
public class EnvironmentSetting 
{
	private String scenarioID;

	@Before
	public void setup(Scenario scenario) throws IOException
	{
		System.out.println(scenario.getId());
		System.out.println(scenario.getName());
		System.setProperty(UIUtils.SCENARIO_ID, Integer.toString(scenario.getId().hashCode()));
		this.scenarioID=scenario.getName()+"_"+Integer.toString(scenario.getId().hashCode());
	}
	
	@After
	public void runHook(Scenario scenario)
	{
		TestEnvironment.get().stopVideoRecording(scenario);
		
		if (scenario.isFailed()){
			takeScreenshotOnFailure(scenario);
		}else
		{
			embedScreenshot(scenario);
		}
		
		closeDriver();
	}
	
	private void closeDriver() 
	{
		EnvironmentSettings s=TestEnvironment.get().getEnvironmentSettings();
	    WebDriver d=TestEnvironment.get().getDriver(s.getDriver());
	    
	    if(d != null)
	    
	    	if (d.getClass().equals(Costants.ANDROID)) {
		    	d.close(); 
			}else {
				d.quit();
			}
	}

	private void takeScreenshotOnFailure(Scenario scenario){
		try {
			WebDriver currentDriver = UIUtils.ui().getCurrentDriver();
			String fileName = UIUtils.ui().takeScreenshot(currentDriver);
			scenario.write("<img src=\"embeddings\\" + fileName + " \" >");
    	} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void embedScreenshot(Scenario scenario){
		int scenarioID = scenario.getId().hashCode();
		String reportImagesDir = System.getProperty(UIUtils.REPORT_IMAGES_DIR);
		try {
			List<Path> listImagesFile = Files.walk(Paths.get(reportImagesDir))
			 .filter(Files::isRegularFile)
			 .filter(f -> StringUtils.contains(f.getFileName().toString(), Integer.toString(scenarioID)))
			 .collect(toList());
			for (Path path : listImagesFile) {
				scenario.write("<img src=\"embeddings\\" + path.toFile().getName() + " \" >");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Given("^this environment configuration$")
	public void this_environment_configuration(List<EnvironmentSettings> settings) throws Throwable 
	{
		EnvironmentSettings setting=settings.get(0);
		setting.setScenarioId(this.scenarioID);
		TestEnvironment.get().setEnvironment(setting);
		
	    //throw new PendingException();
		
		//TestEnvironment.get().startVideoRecording();
	}
	
	@Then("^Close the browser and clean all resources$")
	public void close_the_browser_and_clean_all_resources() throws Throwable 
	{
	    closeDriver();
	    
	    //throw new PendingException();
	}
}
