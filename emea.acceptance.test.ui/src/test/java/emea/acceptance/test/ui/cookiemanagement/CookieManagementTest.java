package emea.acceptance.test.ui.cookiemanagement;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;

@RunWith(XCucumber.class)
@CucumberOptions(plugin = { "json:target/reports/CookieManagement.json" },
features = {"src/test/resources/features/cookie_management/Cookie_management_v1.feature"},
glue= {"classpath:emea/acceptance/test/ui/features"},
tags = {"@firefox"})
@Ui

public class CookieManagementTest {

}
