package emea.acceptance.test.ui.page404;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;

@RunWith(XCucumber.class)
@CucumberOptions(plugin = { "json:target/reports/Page404Test.json" },
features = {"src/test/resources/features/404/404_page_v1.feature"},
glue= {"classpath:emea/acceptance/test/ui/features"},
tags = {"@iexplorer"})
@Ui
public class Page404Test 
{
	
}
