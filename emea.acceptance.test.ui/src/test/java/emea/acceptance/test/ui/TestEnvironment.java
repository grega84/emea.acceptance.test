package emea.acceptance.test.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.Reader;
import java.lang.reflect.Constructor;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cucumber.api.Scenario;
import emea.automation.core.ui.common.datatable.EnvironmentSettings;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;
import ui.core.support.Costants;
import ui.core.support.page.Page;
import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.LocatorRepo;
import ui.core.support.uiobject.repository.PageRepo;

public class TestEnvironment 
{
	private static TestEnvironment singleton;
	private static final Logger logger = LoggerFactory.getLogger(TestEnvironment.class);
	
	private static final String DT_EMEA_CH_DE = "dt_emea_ch_de";
	private static final String DT_EMEA_CH = "dt_emea_ch";
	
	private static final String LOCATORS_FOLDER = "locators";
	private static final String LANGUAGES_FOLDER = "languages";
	private static final String LOADERS_FOLDER = "loaders";
	private static final String MOLECULES_FILE = "molecules.csv";
	private static final String CLASS_CSV_FIELD = "class";
	private static final String CSV_CSV_FIELD = "csv";
	private static final String MOLECULES_FOLDER = "molecules";
	private static final String KEY_CSV_FIELD = "key";
	private static final String PAGES_FILE = "pages.csv";
	private static final String DT_EMEA_CH_DE_IE = "dt_emea_ch_de_ie";
	
	private String testId;
	private String emeaUrlSite;
	private HashMap<String,WebDriver> drivers;
	private HashMap<String, Properties> languages;
	
	private EnvironmentSettings settings;
	
	private String baseVideoNamePath;
	private String videoName;
	
	private TestEnvironment()
	{
		testId=null;
		drivers = new HashMap<>();
		languages = new HashMap<>();
		
	}
	
	public static TestEnvironment get()
	{
		if(singleton == null)
			singleton = new TestEnvironment();
		
		return singleton;
	}

	/**
	 * metodo di inizializzazione ambiente
	 * @param setting
	 */
	public void setEnvironment(EnvironmentSettings setting) 
	{
		this.settings=setting;
		
		this.baseVideoNamePath=this.settings.getScenarioId()+"-"+this.settings.getTestIdJira()+"-"+this.settings.getDriver().toUpperCase();
		
		switch(setting.getSite())
		{
		case DT_EMEA_CH:
			this.emeaUrlSite=EMEASites.DT_EMEA_CH;
			break;
		case DT_EMEA_CH_DE:
			this.emeaUrlSite=EMEASites.DT_EMEA_CH_DE;
			break;
		case DT_EMEA_CH_DE_IE: 
			this.emeaUrlSite=EMEASites.DT_EMEA_CH_DE_IE;
			break;
		}
		
		//carico il driver
		String driver=setting.getDriver();
		
		drivers.put(driver, UIUtils.ui().driver(driver));
		
		File[] listOfFiles;
		//carico i languages
		try {
			listOfFiles = EmeaUtils.getFilesFromFolder(TestEnvironment.class.getClassLoader().getResource(LANGUAGES_FOLDER).toURI());
			
			for(File file : listOfFiles)
			{
				Properties p=new Properties();
				p.load(new FileInputStream(file));
				languages.put(FilenameUtils.removeExtension(file.getName()), p);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		
		//caricamento locators
		LocatorRepo.get().clearRepository();
		
		try {
			listOfFiles = EmeaUtils.getFilesFromFolder(TestEnvironment.class.getClassLoader().getResource(LOCATORS_FOLDER).toURI());
			ArrayList<String> files=new ArrayList<>();
			
			for (File file : listOfFiles) {
			    if (file.isFile()) {
			    	System.out.println(file.getAbsolutePath());
			        files.add(file.getAbsolutePath());
			    }
			}
			
			LocatorRepo.get().loadElements((String[]) files.toArray(new String[files.size()]));
			//System.out.println(LocatorRepo.get().getLocator("gotoHomePage404"));
			
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		//carico le molecole, organismi e pagine
		loadMolecules();
		
		loadPages();
	}
	
	private void loadPages() 
	{
		try {
			Path file=Paths.get(TestEnvironment.class.getClassLoader().getResource(LOADERS_FOLDER+FileSystems.getDefault().getSeparator()+PAGES_FILE).toURI());
			Reader reader=null;
			CSVParser csvParser=null;
			
			reader = Files.newBufferedReader(file);
			
			csvParser= new CSVParser(reader, CSVFormat.EXCEL
					.withFirstRecordAsHeader()
					.withDelimiter(';')
                    .withIgnoreHeaderCase()
                    .withTrim());
			
			for (CSVRecord csvRecord : csvParser)
			{
				String _class=csvRecord.get(CLASS_CSV_FIELD);
				String csv=csvRecord.get(CSV_CSV_FIELD);
				String key=csvRecord.get(KEY_CSV_FIELD);
				
				Class<?> clazz=Class.forName(_class);
				Constructor<?> constructor=clazz.getConstructor(WebDriver.class,Properties.class);
				Page o=(Page) constructor.newInstance(drivers.get(settings.getDriver()),languages.get(settings.getLanguage()));
				
				o.init();
				
				PageRepo.get().set(key, o);
			}
			
			csvParser.close();
			reader.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	private void loadMolecules() 
	{
		try {
			Path file=Paths.get(TestEnvironment.class.getClassLoader().getResource(LOADERS_FOLDER+FileSystems.getDefault().getSeparator()+MOLECULES_FILE).toURI());
			Reader reader=null;
			CSVParser csvParser=null;
			
			reader = Files.newBufferedReader(file);
			
			csvParser= new CSVParser(reader, CSVFormat.EXCEL
					.withFirstRecordAsHeader()
					.withDelimiter(';')
                    .withIgnoreHeaderCase()
                    .withTrim());
			
			for (CSVRecord csvRecord : csvParser)
			{
				String _class=csvRecord.get(CLASS_CSV_FIELD);
				String csv=csvRecord.get(CSV_CSV_FIELD);
				
				Class<?> clazz=Class.forName(_class);
				Constructor<?> constructor=clazz.getConstructor(WebDriver.class,Properties.class);
				Molecule o=(Molecule) constructor.newInstance(drivers.get(settings.getDriver()),languages.get(settings.getLanguage()));
				
				o.init();
				
				//carico da csv
				o.loadFromCsv(Paths.get(TestEnvironment.class.getClassLoader().getResource(MOLECULES_FOLDER+FileSystems.getDefault().getSeparator()+csv).toURI()).toString());
			}
			
			csvParser.close();
			reader.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	public WebDriver getDriver(String config)
	{
		return drivers.get(config);
	}
	
	public Properties getLanguage(String name)
	{
		return languages.get(name);
	}
	
	public EnvironmentSettings getEnvironmentSettings()
	{
		return this.settings;
	}
	
	public void startVideoRecording(String config)
	{
		WebDriver d=drivers.get(settings.getDriver());
		
		if(d != null)
		{
			Class<?> type=d.getClass();
			
			if(!type.equals(Costants.ANDROID))
			{
				videoName=UIUtils.ui().videoRecoder(this.baseVideoNamePath,settings.getDriver());
				UIUtils.ui().startVideoRecording();
			}
			else
			{
				videoName=UIUtils.ui().mobileVideoRecorder(this.baseVideoNamePath,config);
				
				((AndroidDriver) d).context("NATIVE_APP");
				
			    UIUtils.ui().startMobileVideoRecording((AppiumDriver<?>) d);
			    
			    ((AndroidDriver) d).context("CHROMIUM");
			}
		}
	}
	
	public void stopVideoRecording(Scenario scenario)
	{
		WebDriver d=drivers.get(settings.getDriver());
		
		if(d != null)
		{
			Class<?> type=d.getClass();
			
			if(!type.equals(Costants.ANDROID))
			{
				UIUtils.ui().stopVideoRecording();
			}
			else
			{
				((AndroidDriver) d).context("NATIVE_APP");
				
			    UIUtils.ui().stopMobileVideoRecording();
			    
			    ((AndroidDriver) d).context("CHROMIUM");
			}
		}
		
		scenario.write(UIUtils.ui().makeVideoEmbendedDiv(videoName));
	}

	public String getEmeaUrlSite() {
		return emeaUrlSite;
	}
	
}
