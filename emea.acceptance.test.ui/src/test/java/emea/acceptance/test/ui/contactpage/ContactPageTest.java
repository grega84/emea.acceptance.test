package emea.acceptance.test.ui.contactpage;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;

@RunWith(XCucumber.class)
@CucumberOptions(plugin = { "json:target/reports/ContactPageTest.json" },
features = {"src/test/resources/features/contact_page/Contact_page_v1.feature"},
glue= {"classpath:emea/acceptance/test/ui/features"},
tags = {"@chrome"})
@Ui
public class ContactPageTest 
{

}
