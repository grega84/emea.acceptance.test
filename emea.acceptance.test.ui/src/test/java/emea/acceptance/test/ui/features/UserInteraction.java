package emea.acceptance.test.ui.features;

import java.util.List;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.*;
import emea.acceptance.test.ui.TestEnvironment;
import emea.automation.core.ui.common.ElementsName;
import emea.automation.core.ui.common.datatable.FormField;
import emea.automation.core.ui.common.datatable.PageId;
import emea.automation.core.ui.pages.contactus.ContactUsPage;
import emea.automation.core.ui.pages.homepage.HomePage;
import emea.automation.core.ui.pages.page404.Page404;
import ui.core.support.uiobject.repository.PageRepo;
import emea.automation.core.ui.pages.cookiemanagement.CookieManagementPage;

/**
 * classe contenenti le implementazioni realtive ai feature di interazione utente
 * @author Francesco
 *
 */
public class UserInteraction 
{
	@When("^The user clicks on \"([^\"]*)\" from the page$")
	public void the_user_clicks_on_from_the_page(String objectKey, List<PageId> pages) throws Throwable 
	{
	    if(pages.get(0).getPageId().equals(ElementsName.Pages.PAGE_404))
	    {
	    	Page404 p=(Page404) PageRepo.get().get(ElementsName.Pages.PAGE_404);
	    	
	    	p.clickOn(objectKey);
	    }
	    else if(pages.get(0).getPageId().equals(ElementsName.Pages.HOMEPAGE))
	    {
	    	HomePage p=(HomePage) PageRepo.get().get(ElementsName.Pages.HOMEPAGE);
	    	
	    	p.clickOn(objectKey);
	    }
	    else if(pages.get(0).getPageId().equals(ElementsName.Pages.CONTACTUS))
	    {
	    	ContactUsPage p=(ContactUsPage) PageRepo.get().get(ElementsName.Pages.CONTACTUS);
	    	
	    	p.clickOn(objectKey);
	    } else if (pages.get(0).getPageId().equals(ElementsName.Pages.COOKIE_MANAGEMENT)) {
	          CookieManagementPage p=(CookieManagementPage) PageRepo.get().get(ElementsName.Pages.COOKIE_MANAGEMENT);
	          p.clickOn(objectKey);
	          
	          try {Thread.sleep(2000);} catch(Exception err) {}
		    }
	    
	}
	
	@When("^The user clicks each MORE in the family rows from the page$")
	public void the_user_clicks_each_MORE_in_the_family_rows_from_the_page(List<PageId> pages) throws Throwable 
	{
	    HomePage home=(HomePage) PageRepo.get().get(ElementsName.Pages.HOMEPAGE);
	    
	    //click sui link di family row
	    home.clickMoreOnFamilyRow();
	}
	

	@When("^The user clicks each LANGUAGES in the dropdown menu on the footer$")
	public void the_user_clicks_each_languages_in_the_dropdown_menu_on_the_footer(List<PageId> pages) throws Throwable {

		HomePage home=(HomePage) PageRepo.get().get(ElementsName.Pages.HOMEPAGE);

		// Click sul cambio lingua
		home.clickOnChangeLanguage();
	}

	@And("^The user compiles the form with the followings fields$")
	public void the_user_compiles_the_form_with_the_followings_fields(List<FormField> fields) throws Throwable 
	{
		ContactUsPage p=(ContactUsPage) PageRepo.get().get(ElementsName.Pages.CONTACTUS);

		p.compileFormWith(fields.get(0));
	}

	@And("^The user checks the \"([^\"]*)\" and \"([^\"]*)\" checkbox$")
	public void the_user_checks_the_something_and_something_checkbox(String check1, String check2,List<PageId> pages) throws Throwable {
		throw new PendingException();
	}

	@And("^The user dechecks the \"([^\"]*)\" and \"([^\"]*)\" checkbox$")
	public void the_user_dechecks_the_something_and_something_checkbox(String check1, String check2,List<PageId> pages) throws Throwable {
		throw new PendingException();
	}

}
