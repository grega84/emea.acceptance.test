package emea.acceptance.test.ui.features;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.*;
import emea.acceptance.test.ui.TestEnvironment;
import emea.automation.core.ui.common.ElementsName;
import emea.automation.core.ui.common.datatable.EnvironmentSettings;
import emea.automation.core.ui.common.datatable.Page404WrongUrl;
import emea.automation.core.ui.molecule.CookieManagement;
import emea.automation.core.ui.pages.cookiemanagement.CookieManagementPage;
import emea.automation.core.ui.pages.homepage.HomePage;
import emea.automation.core.ui.pages.page404.Page404;
import ui.core.support.Costants;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import emea.automation.core.ui.common.datatable.BreadcrumbsNavigation;
import emea.automation.core.ui.common.datatable.CookiePolicyPage;
import emea.automation.core.ui.common.datatable.PageId;	
import emea.automation.core.ui.common.datatable.BreadcrumbsNavigation;
import emea.automation.core.ui.molecule.Breadcrumbs;
import emea.automation.core.ui.common.datatable.PageId;
import emea.automation.core.ui.molecule.Breadcrumbs;
/**
 * classe contenenti le implementazioni realtive ai feature di navigazione
 * @author Francesco
 *
 */
public class Navigation 
{
	private static final String HOMEPAGE = "home";	
	private static final String BAP = "bookinganapp";	
	private static final String STORE = "storesearch";	
	private static final String HRT = "hrtest";	
	
	EnvironmentSettings settings;
	WebDriver driver;
	
	@Given("^The user goes to DT EMEA site$")
	public void the_user_goes_to_DT_EMEA_site() throws Throwable 
	{
	    settings=TestEnvironment.get().getEnvironmentSettings();
	    driver=TestEnvironment.get().getDriver(settings.getDriver());
	    
	    driver.get(TestEnvironment.get().getEmeaUrlSite());
	    
	    if (driver.getClass().equals(Costants.ANDROID)) {
	    	
		} else {
			driver.manage().window().maximize();
		}
	   
	    CookieManagementPage cookie=(CookieManagementPage) PageRepo.get().get(ElementsName.Pages.COOKIE_MANAGEMENT).get();
	    try {Thread.sleep(3000);} catch(Exception err) {}
	    
	    //chiudo il cookie
	    cookie.closeCookie();
	}
	
	@Given("^The user inserts in the URL a WRONGKEY from the page$")
	public void the_user_inserts_in_the_URL_a_WRONGKEY_from_the_page(List<Page404WrongUrl> urls) throws Throwable 
	{
	    if(settings == null)
	    	settings=TestEnvironment.get().getEnvironmentSettings();
	    if(driver == null)
	    	driver=TestEnvironment.get().getDriver(settings.getDriver());
	    
	    driver.get(TestEnvironment.get().getEmeaUrlSite()+urls.get(0).getWrongKey());
	}
	
	@Given("^The (\\d+) page not found page is displayed$")
	public void the_page_not_found_page_is_displayed(int httpPageCode) throws Throwable 
	{
	    Page404 p404=(Page404) PageRepo.get().get(ElementsName.Pages.PAGE_404).get();
	}
	
	@Then("^The homepage is displayed$")
	public void the_homepage_is_displayed() throws Throwable 
	{
		try {Thread.sleep(3000);} catch(Exception err) {}
		PageRepo.get().get(ElementsName.Pages.HOMEPAGE).get();
	}
	
	@Then("^The Contact us page is displayed$")
	public void the_Contact_us_page_is_displayed() throws Throwable 
	{
		try {Thread.sleep(3000);} catch(Exception err) {}
		PageRepo.get().get(ElementsName.Pages.CONTACTUS).get();
	}
	
	@Then("^The user lands to destination page$")
	public void the_user_lands_to_destination_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}
	
	@When("^The user scrolls to the footer$")
    public void the_user_scrolls_to_the_footer() throws Throwable 
	{
        HomePage home=(HomePage) PageRepo.get().get(ElementsName.Pages.HOMEPAGE);
        
        home.scrollToFooter();
    }
	
	/*	
	 * Breadcrumbs	
	 */	
		
   @When("^The user goes to first level navigation pages from the page$")	
   public void the_user_goes_to_first_level_navigation_pages_from_the_page(List<BreadcrumbsNavigation> breads) throws Throwable 	
   {	
        BreadcrumbsNavigation n=breads.get(0);	
        HomePage home=(HomePage) PageRepo.get().get(ElementsName.Pages.HOMEPAGE);	
        	
        switch(n.getHeaderLink())	
        {	
        case BAP:	
        	home.gotoBookingAnAppointment();	
        	break;	
        case STORE:	
        	home.gotoStoreLocator();	
        	break;	
        case HRT:	
        	home.gotoHearingTest();	
        	break;	
        }	
   }	
   	
   @Then("^No breadcrumbs is displayed on the top of the page$")	
    public void no_breadcrumbs_is_displayed_on_the_top_of_the_page(List<PageId> pageId) throws Throwable 	
   {	
	   Breadcrumbs br=(Breadcrumbs) UiObjectRepo.get().get(Breadcrumbs.NAME);	
			
		boolean find=false;	
			
		try	
		{	
			br.get();	
			find=true;	
		}	
		catch(Exception err)	
		{	
			find=false;	
		}	
			
		if(find)	
			throw new Error("il breadcrump è presente sulla homepage");	
	}	
	
   	
   @When("^The user is on the homepage$")	
   public void the_user_is_on_the_homepage() throws Throwable 	
   {	
	   HomePage home=(HomePage) PageRepo.get().get(ElementsName.Pages.HOMEPAGE);	
	   home.get();	
   }
   
   /*
    * Cookie
    */
   
   @Then("^The cookie banner is closed$")
   public void the_cookie_banner_is_closed() throws Throwable {
      boolean find = false;
      try {
    	  Particle particle = (Particle) UiObjectRepo.get().get(ElementsName.CookieManagement.COOKIEDIV);
    	  particle.get();
    	  find = true;
      } catch (Exception err) {
    	  find = false;
      }
       if(!find) {
    	   throw new Error("Pagina Cookie ancora visibile");
       }
    
   }
   @Then("^The user lands on cookie policy page$")
   public void the_user_lands_on_cookie_policy_page(List<CookiePolicyPage> cookie) throws Throwable {
	  CookiePolicyPage link = cookie.get(0);
	  EnvironmentSettings s=TestEnvironment.get().getEnvironmentSettings();
	  WebDriver d=TestEnvironment.get().getDriver(s.getDriver());
       Assert.assertTrue("L'URL mostrato non è quello di Cookie Policy", d.getCurrentUrl().equals(link));
   }
   
   @When("^The user goes to DT EMEA site in incognito mode$")
   public void the_user_goes_to_dt_emea_site_in_incognito_mode() throws Throwable {
	   settings=TestEnvironment.get().getEnvironmentSettings();
	    driver=TestEnvironment.get().getDriver(settings.getDriver());
	    
	    driver.get(TestEnvironment.get().getEmeaUrlSite());
	    
	    if (driver.getClass().equals(Costants.ANDROID)) {
	    	
		} else {
			driver.manage().window().maximize();
		}
   }
   
}
