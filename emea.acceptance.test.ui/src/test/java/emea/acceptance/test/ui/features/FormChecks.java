package emea.acceptance.test.ui.features;

import java.util.List;

import cucumber.api.PendingException;
import cucumber.api.java.en.*;
import emea.automation.core.ui.common.ElementsName;
import emea.automation.core.ui.common.datatable.ContactFormError;
import emea.automation.core.ui.common.datatable.FormField;
import emea.automation.core.ui.pages.contactus.ContactUsPage;
import ui.core.support.uiobject.repository.PageRepo;

public class FormChecks 
{
	@Then("^For all mandatory fields error messages are displayed$")
    public void for_all_mandatory_fields_error_messages_are_displayed(List<ContactFormError> fields) throws Throwable 
	{
		ContactUsPage p=(ContactUsPage) PageRepo.get().get(ElementsName.Pages.CONTACTUS);
		
		p.checkErrorMessage(fields.get(0));
    }
	
	 @Then("^The form field is editable$")
    public void the_form_field_is_editable(List<FormField> fields) throws Throwable 
	{
		 ContactUsPage p=(ContactUsPage) PageRepo.get().get(ElementsName.Pages.CONTACTUS);
		 
		 p.checkFormFieldIsEditable(fields.get(0));
    }
	
	@Then("^The user checks and dechecks the POLICY and INFORMATIVE checkbox$")
	public void the_user_checks_and_dechecks_the_policy_and_informative_checkbox() throws Throwable 
	{
		ContactUsPage p=(ContactUsPage) PageRepo.get().get(ElementsName.Pages.CONTACTUS);
		
		p.checkPolicyAndInformative();
	}
}
