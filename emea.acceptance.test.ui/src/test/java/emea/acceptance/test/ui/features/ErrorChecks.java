package emea.acceptance.test.ui.features;

import java.util.List;

import cucumber.api.PendingException;
import cucumber.api.java.en.*;
import emea.automation.core.ui.common.ElementsName;
import emea.automation.core.ui.common.datatable.ContactFormError;
import emea.automation.core.ui.common.datatable.FormField;
import emea.automation.core.ui.pages.contactus.ContactUsPage;
import ui.core.support.uiobject.repository.PageRepo;

public class ErrorChecks 
{
	@Then("^An error message is displayed under the Contact form field$")
    public void an_error_message_is_displayed_under_the(List<ContactFormError> fields) throws Throwable 
	{
		ContactUsPage p=(ContactUsPage) PageRepo.get().get(ElementsName.Pages.CONTACTUS);
		
		p.checkErrorMessage(fields.get(0));
    }
}
