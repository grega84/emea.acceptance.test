package emea.acceptance.test.ui.breadcrump;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;

@RunWith(XCucumber.class)
@CucumberOptions(plugin = { "json:target/reports/Breadcrumb.json" },
features = {"src/test/resources/features/breadcrump/Breadcrumbs_v1.feature"},
glue= {"classpath:emea/acceptance/test/ui/features"},
tags = {"@chrome"})
@Ui
public class BreadcrumbTest 
{

}
