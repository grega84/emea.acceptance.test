package emea.acceptance.test.ui.homepage;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;

@RunWith(XCucumber.class)
@CucumberOptions(plugin = { "json:target/reports/HomePageTest.json" },
features = {"src/test/resources/features/homepage/Homepage_v1.feature"},
glue= {"classpath:emea/acceptance/test/ui/features"},
tags = {})
@Ui
public class HomePageTest {

}
