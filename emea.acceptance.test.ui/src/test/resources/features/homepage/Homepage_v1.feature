Feature: Homepage: The user accesses to the Homepage

  Scenario Outline: UUX-1784_Verify the CTA in each of the Family Row
    Given this environment configuration
      | testIdJira   | driver   | site   | language   |
      | <testIdJira> | <driver> | <site> | <language> |
    And The user goes to DT EMEA site
    When The user clicks each MORE in the family rows from the page
      | pageId   |
      | <pageId> |
    And Close the browser and clean all resources
	
	@android
    Examples: 
      | testIdJira | destination | driver 	 | site       	 | language |
      | UUX-1784   | destination | androidS8 | dt_emea_ch_de | language |
 
    @chrome
    Examples: 
      | testIdJira | destination | driver | site       	  | language |
      | UUX-1784   | destination | chrome | dt_emea_ch_de | language |

    @firefox
    Examples: 
      | testIdJira | destination | driver | site       	  | language |
      | UUX-1784   | destination | firefox | dt_emea_ch_de | language |

    @iexplorer
    Examples: 
      | testIdJira | destination | driver | site       	  | language |
      | UUX-1784   | destination | iexplorer | dt_emea_ch_de | language |

    @safari
    Examples: 
      | testIdJira | destination | driver | site       	  | language |
      | UUX-1784   | destination | safari | dt_emea_ch_de | language |


# Test sul cambio di lingua non ancora implementato
# 
# Scenario Outline: Homepage  language switch editorial links check
#	Given this environment configuration
#      | testIdJira   | driver   | site   | language   |
#      | <testIdJira> | <driver> | <site> | <language> |
#    And The user goes to DT EMEA site
#    When The user clicks each LANGUAGES in the dropdown menu on the footer
#      | pageId   |
#      | <pageId> |
#    And Close the browser and clean all resources
#	
#    Examples: 
#      | testIdJira | destination | driver 		| site       	| language |
#      | UUX-1789   | destination | androidHonor | dt_emea_ch_de | language |