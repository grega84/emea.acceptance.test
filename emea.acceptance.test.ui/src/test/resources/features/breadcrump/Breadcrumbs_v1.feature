Feature: Breadcrumb: The path of the site the user is visiting

  Scenario Outline: UUX-368_Verify the view map link in the store detail page
    Given this environment configuration
      | testIdJira   | driver   | site   | language   |
      | <testIdJira> | <driver> | <site> | <language> |
    And The user goes to DT EMEA site
    When The user goes to first level navigation pages from the page
      | pageId |headerLink|
      | <page> |<headerLink>|
    Then No breadcrumbs is displayed on the top of the page
      | pageId |
      | <page> |
    And Close the browser and clean all resources

@android
    Examples: 
      | testIdJira | driver | site          | language | pageId |headerLink   |
      | UUX-368    | androidS8 | dt_emea_ch_de | ch 	   | T-001  |home	      |
      | UUX-368    | androidS8 | dt_emea_ch_de | ch 	   | T-001  |bookinganapp |
      | UUX-368    | androidS8 | dt_emea_ch_de | ch 	   | T-001  |storesearch  |
      | UUX-368    | androidS8 | dt_emea_ch_de | ch 	   | T-001  |hrtest       |

@chrome
    Examples: 
      | testIdJira | driver | site          | language | pageId |headerLink   |
      | UUX-368    | chrome | dt_emea_ch_de | ch 	   | T-001  |home	      |
      | UUX-368    | chrome | dt_emea_ch_de | ch 	   | T-001  |bookinganapp |
      | UUX-368    | chrome | dt_emea_ch_de | ch 	   | T-001  |storesearch  |
      | UUX-368    | chrome | dt_emea_ch_de | ch 	   | T-001  |hrtest       |
      
@firefox
    Examples: 
      | testIdJira | driver | site          | language | pageId |headerLink   |
      | UUX-368    | firefox | dt_emea_ch_de | ch 	   | T-001  |home	      |
      | UUX-368    | firefox | dt_emea_ch_de | ch 	   | T-001  |bookinganapp |
      | UUX-368    | firefox | dt_emea_ch_de | ch 	   | T-001  |storesearch  |
      | UUX-368    | firefox | dt_emea_ch_de | ch 	   | T-001  |hrtest       |
      
@iexplorer
    Examples: 
      | testIdJira | driver | site          | language | pageId |headerLink   |
      | UUX-368    | iexplorer | dt_emea_ch_de | ch 	   | T-001  |home	      |
      | UUX-368    | iexplorer | dt_emea_ch_de | ch 	   | T-001  |bookinganapp |
      | UUX-368    | iexplorer | dt_emea_ch_de | ch 	   | T-001  |storesearch  |
      | UUX-368    | iexplorer | dt_emea_ch_de | ch 	   | T-001  |hrtest       |

@safari
    Examples: 
      | testIdJira | driver | site          | language | pageId |headerLink   |
      | UUX-368    | safari | dt_emea_ch_de | ch 	   | T-001  |home	      |
      | UUX-368    | safari | dt_emea_ch_de | ch 	   | T-001  |bookinganapp |
      | UUX-368    | safari | dt_emea_ch_de | ch 	   | T-001  |storesearch  |
      | UUX-368    | safari | dt_emea_ch_de | ch 	   | T-001  |hrtest       |
            
  Scenario Outline: UUX-694_Verify that the breadcrumb is not shown in the homepage
    Given this environment configuration
      | testIdJira   | driver   | site   | language   |
      | <testIdJira> | <driver> | <site> | <language> |
    And The user goes to DT EMEA site
    When The user is on the homepage
    Then No breadcrumbs is displayed on the top of the page
      | pageId |
      | <page> |
    And Close the browser and clean all resources

@android
    Examples: 
      | testIdJira | driver | site          | language | pageId |
      | UUX-368    | androidS8 | dt_emea_ch_de | ch 	   | T-001  |
@chrome
    Examples: 
      | testIdJira | driver | site          | language | pageId |
      | UUX-368    | chrome | dt_emea_ch_de | ch 	   | T-001  |
@firefox
    Examples: 
      | testIdJira | driver | site          | language | pageId |
      | UUX-368    | firefox | dt_emea_ch_de | ch 	   | T-001  |
@iexplorer
    Examples: 
      | testIdJira | driver | site          | language | pageId |
      | UUX-368    | iexplorer | dt_emea_ch_de | ch 	   | T-001  |
@safari
    Examples: 
      | testIdJira | driver | site          | language | pageId |
      | UUX-368    | safari | dt_emea_ch_de | ch 	   | T-001  |
      