Feature: 404 page: Error for page not found

  Scenario Outline: UUX-1757_Verify the homepage link in the 404 page
    Given this environment configuration
      | testIdJira   | driver   | site   | language   |
      | <testIdJira> | <driver> | <site> | <language> |
    And The user goes to DT EMEA site
    And The user inserts in the URL a WRONGKEY from the page
      | pageId   | wrongKey   |
      | <pageId> | <wrongKey> |
    And The 404 page not found page is displayed
    When The user clicks on "HOMEPAGE" from the page
      | pageId   |
      | <pageId> |
    Then The homepage is displayed
    And Close the browser and clean all resources
	
	@android
    Examples: 
      | testIdJira | pageId  | wrongKey | driver	| site          | language |
      | UUX-1757   | 404Page | pippo    | androidS8 | dt_emea_ch_de | language |

	@chrome
    Examples: 
      | testIdJira | pageId  | wrongKey | driver | site          | language |
      | UUX-1757   | 404Page | pippo    | chrome | dt_emea_ch_de | language |
      
 	@firefox
    Examples: 
      | testIdJira | pageId  | wrongKey | driver | site          | language |
      | UUX-1757   | 404Page | pippo    | firefox | dt_emea_ch_de | language |  
      
  @iexplorer
    Examples: 
      | testIdJira | pageId  | wrongKey | driver | site          | language |
      | UUX-1757   | 404Page | pippo    | iexplorer | dt_emea_ch_de | language |
      
      
        @safari
    Examples: 
      | testIdJira | pageId  | wrongKey | driver | site          | language |
      | UUX-1757   | 404Page | pippo    | safari | dt_emea_ch_de | language |       

  Scenario Outline: UUX-1757_Verify the homepage link in the 404 page
    Given this environment configuration
      | testIdJira   | driver   | site   |
      | <testIdJira> | <driver> | <site> |
    And The user goes to DT EMEA site
    And The user inserts in the URL a WRONGKEY from the page
      | pageId   | wrongKey   |
      | <pageId> | <wrongKey> |
    And The 404 page not found page is displayed
    When The user clicks on "CONTACT US" from the page
      | pageId   |
      | <pageId> |
    Then The Contact us page is displayed
    And Close the browser and clean all resources
	
	@android
    Examples: 
      | testIdJira | pageId  | wrongKey | driver 	| site          | language |
      | UUX-1757   | 404Page | pippo    | androidS8 | dt_emea_ch_de | language |

    @chrome
    Examples: 
      | testIdJira | pageId  | wrongKey | driver | site          | language |
      | UUX-1757   | 404Page | pippo    | chrome | dt_emea_ch_de | language |
      
   @firefox
    Examples: 
      | testIdJira | pageId  | wrongKey | driver | site          | language |
      | UUX-1757   | 404Page | pippo    | firefox | dt_emea_ch_de | language |
      
      
          @iexplorer
    Examples: 
      | testIdJira | pageId  | wrongKey | driver | site          | language |
      | UUX-1757   | 404Page | pippo    | iexplorer | dt_emea_ch_de | language |
      
@safari
    Examples: 
      | testIdJira | pageId  | wrongKey | driver | site          | language |
      | UUX-1757   | 404Page | pippo    | safari | dt_emea_ch_de | language |