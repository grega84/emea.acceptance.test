Feature: Contact page: The user compiles the form with his information

  Scenario Outline: UUX-393_Verify error messages on click on Submit button in the contact page
    Given this environment configuration
      | testIdJira   | driver   | site   | language   |
      | <testIdJira> | <driver> | <site> | <language> |
    And The user goes to DT EMEA site
    And The user scrolls to the footer
    And The user clicks on "CONTACT US" from the page
      | pageId   |
      | <pageId> |
    And The user clicks on "SEND" from the page
      | pageId |
      | T-011  |
    Then For all mandatory fields error messages are displayed
      | fieldKey   | fieldError   |
      | <fieldKey> | <fieldError> |
    And Close the browser and clean all resources
	
	@android
    Examples: 
      | testIdJira | pageId | fieldKey      | fieldError                                                                       | driver	   | site          | language |
      | UUX-393    | T-001  | contactFormMr | Pflichtfeld.                                                                     | androidS8 | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte geben Sie Ihren Vornamen an.                                               | androidS8 | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte geben Sie Ihren Vornamen an.                                               | androidS8 | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte geben Sie Ihre Email an, damit wir Ihnen eine Bestätigung schicken können. | androidS8 | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte akzeptieren Sie die Datenschutzerklärung.                                  | androidS8 | dt_emea_ch_de | language |

	@chrome
    Examples: 
      | testIdJira | pageId | fieldKey      | fieldError                                                                       | driver	| site          | language |
      | UUX-393    | T-001  | contactFormMr | Pflichtfeld.                                                                     | chrome | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte geben Sie Ihren Vornamen an.                                               | chrome | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte geben Sie Ihren Vornamen an.                                               | chrome | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte geben Sie Ihre Email an, damit wir Ihnen eine Bestätigung schicken können. | chrome | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte akzeptieren Sie die Datenschutzerklärung.                                  | chrome | dt_emea_ch_de | language |
	@firefox
    Examples: 
      | testIdJira | pageId | fieldKey      | fieldError                                                                       | driver	| site          | language |
      | UUX-393    | T-001  | contactFormMr | Pflichtfeld.                                                                     | firefox | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte geben Sie Ihren Vornamen an.                                               | firefox | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte geben Sie Ihren Vornamen an.                                               | firefox | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte geben Sie Ihre Email an, damit wir Ihnen eine Bestätigung schicken können. | firefox | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte akzeptieren Sie die Datenschutzerklärung.                                  | firefox | dt_emea_ch_de | language |
	@iexplorer
    Examples: 
      | testIdJira | pageId | fieldKey      | fieldError                                                                       | driver	| site          | language |
      | UUX-393    | T-001  | contactFormMr | Pflichtfeld.                                                                     | iexplorer | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte geben Sie Ihren Vornamen an.                                               | iexplorer | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte geben Sie Ihren Vornamen an.                                               | iexplorer | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte geben Sie Ihre Email an, damit wir Ihnen eine Bestätigung schicken können. | iexplorer | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte akzeptieren Sie die Datenschutzerklärung.                                  | iexplorer | dt_emea_ch_de | language |
	@safari
    Examples: 
      | testIdJira | pageId | fieldKey      | fieldError                                                                       | driver	| site          | language |
      | UUX-393    | T-001  | contactFormMr | Pflichtfeld.                                                                     | safari | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte geben Sie Ihren Vornamen an.                                               | safari | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte geben Sie Ihren Vornamen an.                                               | safari | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte geben Sie Ihre Email an, damit wir Ihnen eine Bestätigung schicken können. | safari | dt_emea_ch_de | language |
      | UUX-393    | T-001  | contactFormMr | Bitte akzeptieren Sie die Datenschutzerklärung.                                  | safari | dt_emea_ch_de | language |

  Scenario Outline: Verify that the form fields are editable
    #UUX-384_Verify that the user can provide an email address in the contact page
    #UUX-391_Verify that the user can accept the privacy statements on the contact page
    Given this environment configuration
      | testIdJira   | driver   | site   | language   |
      | <testIdJira> | <driver> | <site> | <language> |
    And The user goes to DT EMEA site
    When The user scrolls to the footer
    And The user clicks on "CONTACT US" from the page
      | pageId   |
      | <pageId> |
    Then The form field is editable
      | fieldKey   | fieldValue   |
      | <fieldKey> | <fieldValue> |
    And The user compiles the form with the followings fields
      | fieldKey   | fieldValue   |
      | <fieldKey> | <fieldValue> |
    And Close the browser and clean all resources
	
	@android
    Examples: 
      | testIdJira      | pageId | fieldKey    | fieldValue                                                | driver    | site          | language |
      | UUX-384,UUX-391 | T-001  | FirstName   | fieldValue                                                | androidS8 | dt_emea_ch_de | language |
      | UUX-384,UUX-391 | T-001  | LastName    | fieldValue                                                | androidS8 | dt_emea_ch_de | language |
      | UUX-384,UUX-391 | T-001  | PhoneNumber |                                                 326556568 | androidS8 | dt_emea_ch_de | language |
      | UUX-384,UUX-391 | T-001  | TextArea    | lorem ipsum <a href="https://www.google.it>clicca qui</a> | androidS8 | dt_emea_ch_de | language |
      | UUX-384         | T-001  | Email       | testprova@gmail.com                                       | androidS8 | dt_emea_ch_de | language |
      | UUX-391         | T-001  | Privacy1    | true                                                      | androidS8 | dt_emea_ch_de | language |
	
	@chrome
    Examples: 
      | testIdJira      | pageId | fieldKey    | fieldValue                                                | driver | site          | language |
      | UUX-384,UUX-391 | T-001  | FirstName   | fieldValue                                                | chrome | dt_emea_ch_de | language |
      | UUX-384,UUX-391 | T-001  | LastName    | fieldValue                                                | chrome | dt_emea_ch_de | language |
      | UUX-384,UUX-391 | T-001  | PhoneNumber |                                                 326556568 | chrome | dt_emea_ch_de | language |
      | UUX-384,UUX-391 | T-001  | TextArea    | lorem ipsum <a href="https://www.google.it>clicca qui</a> | chrome | dt_emea_ch_de | language |
      | UUX-384         | T-001  | Email       | testprova@gmail.com                                       | chrome | dt_emea_ch_de | language |
      | UUX-391         | T-001  | Privacy1    | true                                                      | chrome | dt_emea_ch_de | language |
	@firefox
    Examples: 
      | testIdJira      | pageId | fieldKey    | fieldValue                                                | driver | site          | language |
      | UUX-384,UUX-391 | T-001  | FirstName   | fieldValue                                                | firefox | dt_emea_ch_de | language |
      | UUX-384,UUX-391 | T-001  | LastName    | fieldValue                                                | firefox | dt_emea_ch_de | language |
      | UUX-384,UUX-391 | T-001  | PhoneNumber |                                                 326556568 | firefox | dt_emea_ch_de | language |
      | UUX-384,UUX-391 | T-001  | TextArea    | lorem ipsum <a href="https://www.google.it>clicca qui</a> | firefox | dt_emea_ch_de | language |
      | UUX-384         | T-001  | Email       | testprova@gmail.com                                       | firefox | dt_emea_ch_de | language |
      | UUX-391         | T-001  | Privacy1    | true                                                      | firefox | dt_emea_ch_de | language |
	@iexplorer
    Examples: 
      | testIdJira      | pageId | fieldKey    | fieldValue                                                | driver | site          | language |
      | UUX-384,UUX-391 | T-001  | FirstName   | fieldValue                                                | iexplorer | dt_emea_ch_de | language |
      | UUX-384,UUX-391 | T-001  | LastName    | fieldValue                                                | iexplorer | dt_emea_ch_de | language |
      | UUX-384,UUX-391 | T-001  | PhoneNumber |                                                 326556568 | iexplorer | dt_emea_ch_de | language |
      | UUX-384,UUX-391 | T-001  | TextArea    | lorem ipsum <a href="https://www.google.it>clicca qui</a> | iexplorer | dt_emea_ch_de | language |
      | UUX-384         | T-001  | Email       | testprova@gmail.com                                       | iexplorer | dt_emea_ch_de | language |
      | UUX-391         | T-001  | Privacy1    | true                                                      | iexplorer | dt_emea_ch_de | language |
	@safari
    Examples: 
      | testIdJira      | pageId | fieldKey    | fieldValue                                                | driver | site          | language |
      | UUX-384,UUX-391 | T-001  | FirstName   | fieldValue                                                | safari | dt_emea_ch_de | language |
      | UUX-384,UUX-391 | T-001  | LastName    | fieldValue                                                | safari | dt_emea_ch_de | language |
      | UUX-384,UUX-391 | T-001  | PhoneNumber |                                                 326556568 | safari | dt_emea_ch_de | language |
      | UUX-384,UUX-391 | T-001  | TextArea    | lorem ipsum <a href="https://www.google.it>clicca qui</a> | safari | dt_emea_ch_de | language |
      | UUX-384         | T-001  | Email       | testprova@gmail.com                                       | safari | dt_emea_ch_de | language |
      | UUX-391         | T-001  | Privacy1    | true                                                      | safari | dt_emea_ch_de | language |

  Scenario Outline: UUX-390_Verify that the phone number field in the contact page can only contain numbers
    Given this environment configuration
      | testIdJira   | driver   | site   | language   |
      | <testIdJira> | <driver> | <site> | <language> |
    When The user goes to DT EMEA site
    And The user scrolls to the footer
    And The user clicks on "CONTACT US" from the page
      | pageId   |
      | <pageId> |
    And The user compiles the form with the followings fields
      | fieldKey   | fieldValue   |
      | <fieldKey> | <fieldValue> |
    And The user clicks on "SEND" from the page
      | pageId |
      | T-011  |
    Then An error message is displayed under the Contact form field
      | fieldKey   | fieldError   |
      | <fieldKey> | <fieldError> |
    And Close the browser and clean all resources
	
	@android
    Examples: 
      | testIdJira | pageId | fieldKey    | fieldValue | fieldError                                     | driver 	| site          | language |
      | UUX-393    | T-001  | PhoneNumber | fieldValue | Bitte geben Sie eine gültige Telefonnummer an. | androidS8 | dt_emea_ch_de | language |
	
	@chrome
    Examples: 
      | testIdJira | pageId | fieldKey    | fieldValue | fieldError                                     | driver | site          | language |
      | UUX-393    | T-001  | PhoneNumber | fieldValue | Bitte geben Sie eine gültige Telefonnummer an. | chrome | dt_emea_ch_de | language |
	@firefox
    Examples: 
      | testIdJira | pageId | fieldKey    | fieldValue | fieldError                                     | driver | site          | language |
      | UUX-393    | T-001  | PhoneNumber | fieldValue | Bitte geben Sie eine gültige Telefonnummer an. | firefox | dt_emea_ch_de | language |
	@iexplorer
    Examples: 
      | testIdJira | pageId | fieldKey    | fieldValue | fieldError                                     | driver | site          | language |
      | UUX-393    | T-001  | PhoneNumber | fieldValue | Bitte geben Sie eine gültige Telefonnummer an. | iexplorer | dt_emea_ch_de | language |
	@safari
    Examples: 
      | testIdJira | pageId | fieldKey    | fieldValue | fieldError                                     | driver | site          | language |
      | UUX-393    | T-001  | PhoneNumber | fieldValue | Bitte geben Sie eine gültige Telefonnummer an. | safari | dt_emea_ch_de | language |

  Scenario Outline: UUX-391_Verify that the user can accept the privacy statements on the contact page
    Given this environment configuration
      | testIdJira   | driver   | site   | language   |
      | <testIdJira> | <driver> | <site> | <language> |
    When The user goes to DT EMEA site
    And The user scrolls to the footer
    And The user clicks on "CONTACT US" from the page
      | pageId   |
      | <pageId> |
    Then The user checks and dechecks the POLICY and INFORMATIVE checkbox
    And Close the browser and clean all resources
	
	@android
    Examples: 
      | testIdJira | pageId | fieldId | driver 	  | site          | language |
      | UUX-391    | T-001  | fieldId | androidS8 | dt_emea_ch_de | language |

    @chrome
    Examples: 
      | testIdJira | pageId | fieldId | driver 		 | site          | language |
      | UUX-391    | T-001  | fieldId | chrome | dt_emea_ch_de | language |
    @firefox
    Examples: 
      | testIdJira | pageId | fieldId | driver 		 | site          | language |
      | UUX-391    | T-001  | fieldId | firefox | dt_emea_ch_de | language |
     @iexplorer
    Examples: 
      | testIdJira | pageId | fieldId | driver 		 | site          | language |
      | UUX-391    | T-001  | fieldId | iexplorer | dt_emea_ch_de | language |
     @safari
    Examples: 
      | testIdJira | pageId | fieldId | driver 		 | site          | language |
      | UUX-391    | T-001  | fieldId | safari | dt_emea_ch_de | language |
       